
<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
    <form action="/welcome" method = "post">
    @csrf
    <fieldset>
        <legend>SanberBook</legend>
        <label for="fname">First name:</label> 
        <br> <br>
        <input type="text" id="fname" name="fname" placeholder="-First Name-">
        <br> <br>
        <label for="lname">Last name:</label>
        <br> <br>
        <input type="text" id="lname" name="lname" placeholder="-Last Name-">
        <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="Gender" id="pria"> <label for="pria">Pria</label> <br>
        <input type="radio" name="Gender" id="wanita"> <label for="wanita">Wanita</label> <br>
        <input type="radio" name="Gender" id="other"> <label for="other">Other</label> <br> <br>
        <label> Nationality:</label> <br> <br>
        <select name="Nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option> 
        </select> 
        <br> <br>
        <label> Language Spoken:</label> <br> <br>
            <input type="checkbox" name="lang" id="bindo"> <label for="bindo">Bahasa Indonesia</label> <br>
            <input type="checkbox" name="lang" id="english"> <label for="english">English</label><br>
            <input type="checkbox" name="lang" id="other"> <label for="other">Other</label>
        <br><br>
        <label for="bio"> Bio:</label>
        <br> <br>
        <textarea name="bio" id="bio" cols="30" rows="10"> </textarea><br>
        <input type="Submit">
        <br><br>
    </fieldset>
    </form>
    
    </body>
</html>